<?php

/**
 * @file
 * Manufacturing Vendor property info
 */

/**
 * Implementations hook_entity_property_info().
 */
function manufacturing_vendor_entity_property_info() {
  $info = array();

  // Add meta-data about the vendor properties.
  $vendor = &$info['manufacturing_vendor']['properties'];

  $vendor['vid'] = array(
    'type' => 'integer',
    'label' => t('Vendor ID'),
    'description' => t('The unique ID of the manufacturing vendor.'),
    'setter permission' => 'administer manufacturing',
    'schema field' => 'bid',
  );
  $vendor['name'] = array(
    'label' => t('Name'),
    'description' => t('The name of the vendor.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer manufacturing',
    'required' => TRUE,
    'schema field' => 'name',
  );
  $vendor['owner'] = array(
    'type' => 'user',
    'label' => t('Owner'),
    'description' => t('The user who owns the vendor.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer manufacturing',
    'required' => TRUE,
    'schema field' => 'uid',
  );

  return $info;
}

